# Minds Argo CD Applications

This repo contains the Argo CD [ApplicationSet](https://argo-cd.readthedocs.io/en/stable/user-guide/application-set/) resources for deploying the Minds stack using Argo CD.

This repo is itself intended to be deployed as an Argo CD application, using the [app of apps](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/) pattern.

## Structure

Applications are separated by environment, with a `sandbox` and `production` folder. Each app is defined as an `ApplicationSet`, allowing for creating multiple Argo CD apps using basic templating.

## Deploying 

You can add this repository as an [Application](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/) in Argo CD, either through the UI or through the CLI. This requires Argo CD to be deployed in the target cluster. Currently, we use a Helm chart to deploy Argo CD [here](https://gitlab.com/minds/infrastructure/minds-terraform/-/tree/master/environments/argocd).
